#!/bin/bash
npm run build
node format-dist-index.js
mv dist/index.html dist/index.html.erb


cd ../school-rails
git add .
git commit -m "avance `date +'%d/%m/%y %H:%M:%S'`" 
git push


timestamp=`date +%s`
git checkout -b vue-dist-${timestamp} master
git push origin vue-dist-${timestamp}


rm public/js/* 
rm public/css/* 
cp -r ../school-vue/dist/* public/ 
cp ../school-vue/config_favicons/* public/
cp public/index.html.erb app/views/intranet/index.html.erb

git add .
git commit -m "avance `date +'%d/%m/%y %H:%M:%S'`" 
git push --set-upstream origin vue-dist-${timestamp}

