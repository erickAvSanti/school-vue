import Vue from "vue";
import VueRouter from "vue-router";
import NotFound from "../views/NotFound.vue";
import PanelStudent from "../views/PanelStudent.vue";
import PanelStudentPersonalInformation from "../views/PanelStudentPersonalInformation.vue";
import PanelStudentNotificationView from "../views/PanelStudentNotificationView.vue";
import PanelStudentHomeworks from "../views/PanelStudentHomeworks.vue";
import PanelTeacher from "../views/PanelTeacher.vue";
import PanelTeacherSettings from "../views/PanelTeacherSettings.vue";
import PanelTeacherCourse from "../views/PanelTeacherCourse.vue";
import PanelTeacherStudent from "../views/PanelTeacherStudent.vue";
import PanelTeacherStudentView from "../views/PanelTeacherStudentView.vue";
import PanelTeacherTeacher from "../views/PanelTeacherTeacher.vue";
import PanelTeacherTeacherView from "../views/PanelTeacherTeacherView.vue";
import PanelTeacherAcademicYears from "../views/PanelTeacherAcademicYears.vue";
import PanelTeacherAcademicYearsView from "../views/PanelTeacherAcademicYearsView.vue";
//import PanelTeacherStudentNew from "../views/PanelTeacherStudentNew.vue";
import PanelTeacherClassroomCourseView from "../views/PanelTeacherClassroomCourseView.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/panel-student",
    name: "panel_student",
    component: PanelStudent,
  },
  {
    path: "/panel-student/personal-information",
    name: "panel_student_personal_information",
    component: PanelStudentPersonalInformation,
  },
  {
    path: "/panel-student/activities",
    name: "panel_student_homeworks",
    component: PanelStudentHomeworks,
  },
  {
    path: "/panel-student/notifications/:id(\\d+)",
    name: "panel_student_notification_view",
    component: PanelStudentNotificationView,
  },
  {
    path: "/panel-teacher",
    name: "panel_teacher",
    component: PanelTeacher,
  },
  {
    path: "/panel-teacher/settings",
    name: "panel_teacher_settings",
    component: PanelTeacherSettings,
  },
  {
    path: "/panel-teacher/courses",
    name: "panel_teacher_course",
    component: PanelTeacherCourse,
  },
  {
    path: "/panel-teacher/teachers",
    name: "panel_teacher_teacher",
    component: PanelTeacherTeacher,
  },
  {
    path: "/panel-teacher/teachers/:id(\\d+)",
    name: "panel_teacher_teacher_view",
    component: PanelTeacherTeacherView,
  },
  {
    path: "/panel-teacher/students",
    name: "panel_teacher_student",
    component: PanelTeacherStudent,
  },
  {
    path: "/panel-teacher/students/:id(\\d+)",
    name: "panel_teacher_student_view",
    component: PanelTeacherStudentView,
  },
  {
    path: "/panel-teacher/academic-years",
    name: "panel_teacher_academic_years",
    component: PanelTeacherAcademicYears,
  },
  {
    path: "/panel-teacher/academic-years/:id(\\d+)",
    name: "panel_teacher_academic_years_view",
    component: PanelTeacherAcademicYearsView,
  },
  {
    path: "/panel-teacher/classroom-course-view/:classroom_id(\\d+)/:course_id(\\d+)",
    name: "panel_teacher_classroom_course_view",
    component: PanelTeacherClassroomCourseView,
  },
  {
    path: '/panel-*',
    name: "default",
    component: NotFound,
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
