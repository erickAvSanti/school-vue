import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
	state: {
		logged:null,
		profile:{id:null,fullname:'',settings:{
			school_name:'',
			school_motto:'',
		}},
		modals:{
			login:false,
		},
	},
	mutations: {
		websiteTitle(state,str){
			const ss = (str ? (str+' | ') : '') + (state.profile.settings && state.profile.settings.school_name ? state.profile.settings.school_name : '')
			if(ss)window.document.title = ss
		},
		showModalLogin(state){
			state.modals.login = true;
		},
		hideModalLogin(state){
			state.modals.login = false;
		},
		setModalLoginState(state,value){
			state.modals.login = value;
		},
		setLoggedOut(state){
			if(this.$dev_mode)console.log("call setLoggedOut")
			state.logged = false
		},
		setLoggedIn(state){
			if(this.$dev_mode)console.log("call setLoggedIn")
			state.logged = true
		},
		setLoggedState(state,value){
			if(this.$dev_mode)console.log("call setLoggedState, value = ",value)
			state.logged = value
		},
		setProfile(state,value){
			if(this.$dev_mode)console.log("call setProfile, value = ",value)
			state.profile = value
			this.commit('websiteTitle')
		},
	},
	actions: {},
	modules: {}
});
