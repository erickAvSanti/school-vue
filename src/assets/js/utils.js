
export function pag(current,total){
  let arr = []
  const push2=function(_arr,val){
    let found = false
    for(let i=0;i<_arr.length;i++){
      if(_arr[i]==val && val!='...'){
        found=true
        break
      }
    }
    if(!found)_arr.push(val)
    return found
  };
  if(total>7){
    
    if(current<5){
      for(let i=1;i<5;i++){
        push2(arr,i)
      }
      push2(arr,5)
      push2(arr,'...')
      push2(arr,total-2)
      push2(arr,total-1)
      push2(arr,total)
    }else if(current>total-4){
      push2(arr,1)
      push2(arr,2)
      push2(arr,3)
      push2(arr,4)
      push2(arr,'...')
      for(let i=total-4;i<=total;i++){
        push2(arr,i)
      }
    }else{
      push2(arr,1)
      push2(arr,2)
      push2(arr,3)
      push2(arr,4)
      push2(arr,'...')
      push2(arr,current-1)
      push2(arr,current)
      push2(arr,current+1)
      push2(arr,'...')
      push2(arr,total-2)
      push2(arr,total-1)
      push2(arr,total)
    }
  }else{
    for(let i=1;i<=total;i++){
      push2(arr,i)
    }
  }
  const removeIndex = []
  for(let i=0;i<arr.length-2;i++){
    const v1 = arr[i]
    const v2 = arr[i+1]
    const v3 = arr[i+2]
    if(typeof v1=='number' && typeof v3=='number' && v2=='...'){
      if(v3-v1==1){
        removeIndex.push(i+1)
      }
      
    }
  }
  for(let i=arr.length-1;i>=2;i--){
    const v1 = arr[i-2]
    const v2 = arr[i-1]
    const v3 = arr[i]
    if(typeof v1=='number' && typeof v3=='number' && v2=='...'){
      if(v3-v1==1){
        removeIndex.push(i-1)
      }
      
    }
  }
  for(let i=0;i<arr.length;i++){
    const midd = arr[i]
    const v_prev = arr[i-1]
    const v_next = arr[i+1]
    if(typeof v_prev=='number' && typeof v_next=='number' && midd=='...'){
      if(v_next-v_prev==2){
        arr[i] = v_prev+1
      }
      
    }
  }
  if(removeIndex.length>0){
    const arr2 = []
    for(let i=0;i<arr.length;i++){
      let ff = false
      for(let j=0;j<removeIndex.length;j++){
        if(i==removeIndex[j]){
          ff=true
          break
        }
      }
      if(!ff){
        arr2.push(arr[i])
      }
    }
    arr = arr2
  }
  return arr
}
export function get_inputs_flag_edition(arr){
  const arr2 = new Array(arr.length)
  const _length = null
  let pos = 0
  for(const objx of arr){
    const json = {}
    for(const ojby in objx){
      if(typeof ojby == 'number' || typeof ojby == 'string'){
        json[ojby] = false
      }
    }
    arr2[pos] = json
    pos++
  }
  console.log(arr2)
  return arr2
}