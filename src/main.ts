import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";

import { library } from '@fortawesome/fontawesome-svg-core'
import { 
	faEdit,
	faPlus,
	faTimes,
	faPowerOff,
	faPortrait,
	faChevronLeft,
	faChevronRight,
	faAngleDoubleLeft,
	faAngleDoubleRight,
	faSave,
	faSync,
	faFemale,
	faMale,
	faExternalLinkSquareAlt,
	faExternalLinkAlt,
	faEye,
	faEyeSlash,
	faCircle,
	faCheckCircle,
	faSortDown,
	faSortUp,
	faBookReader,
	faUserPlus,
} from '@fortawesome/free-solid-svg-icons'
import { 
	faCircle as farFaCircle,
	faStickyNote as farFaStickyNote,
	faFileImage as farFaFileImage,
	faFilePdf as farFaFilePdf,
	faFileWord as farFaFileWord,
	faFilePowerpoint as farFaFilePowerpoint,
	faPlayCircle as farFaPlayCircle,
	faQuestionCircle as farFaQuestionCircle,
	faArrowAltCircleRight as farFaArrowAltCircleRight,
	faClock as farFaClock,
} from '@fortawesome/free-regular-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

library.add(
	faEdit,
	faPlus,
	faTimes,
	faPowerOff,
	faPortrait,
	faChevronLeft,
	faChevronRight,
	faAngleDoubleLeft,
	faAngleDoubleRight,
	faSave,
	faSync,
	faFemale,
	faMale,
	faExternalLinkSquareAlt,
	faExternalLinkAlt,
	faEye,
	faEyeSlash,
	faCircle,
	faCheckCircle,
	faSortDown,
	faSortUp,
	faBookReader,
	faUserPlus,
	farFaCircle,
	farFaStickyNote,
	farFaFileImage,
	farFaFilePdf,
	farFaFileWord,
	farFaFilePowerpoint,
	farFaPlayCircle,
	farFaQuestionCircle,
	farFaArrowAltCircleRight,
	farFaClock,
)

Vue.component('fas', FontAwesomeIcon)

import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'

// Install BootstrapVue
Vue.use(BootstrapVue)
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin)

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.config.productionTip = false;


import VueMoment from 'vue-moment'
import moment from 'moment-timezone'
Vue.use(VueMoment, {
	moment,
})


import 'bootstrap/dist/css/bootstrap.css';
import 'pc-bootstrap4-datetimepicker/build/css/bootstrap-datetimepicker.css'

import datePicker from 'vue-bootstrap-datetimepicker'
Vue.use(datePicker)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
