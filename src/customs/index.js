exports.install = function(Vue,options){

	Vue.prototype.$getLocalTimeZone = function(dt){
		return this.$moment(dt).tz(this.timezone).format('DD/MM/YYYY HH:mm:ss')
	}
	Vue.prototype.$copy_selection = function(){
		document.execCommand('copy')
	}
	Vue.prototype.timezone = process.env.VUE_APP_TIMEZONE
	Vue.prototype.developer = "Erick avalos"
	Vue.prototype.$time_to_load_list = 1200
	Vue.prototype.$dev_mode = process.env.NODE_ENV === 'development'
	Vue.prototype.$permission_message = "No tienes permiso para realizar esta acción"
	Vue.prototype.$operation_denied_message = "No estás habilitad@ para realizar esta acción"
	Vue.prototype.$session_expired_message = "Su sesión ha expirado"
	Vue.prototype.$unknow_operation_message = "No se pudo procesar esta acción"


}