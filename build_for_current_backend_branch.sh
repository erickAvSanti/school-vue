#!/bin/bash
if [ $# -gt 0 ] && [ $1 == 'development' ]; then
	npm run build-dev
else
	npm run build
fi
node format-dist-index.js
mv dist/index.html dist/index.html.erb

cd ../school-rails
echo $(pwd)
rm public/js/* 
rm public/css/* 
cp -r ../school-vue/dist/* public/
cp ../school-vue/config_favicons/* public/
cp public/index.html.erb app/views/intranet/index.html.erb
