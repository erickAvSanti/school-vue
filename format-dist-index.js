const formatter = require('html-formatter')
const fs = require('fs')

const file_path = 'dist/index.html'
const file_favicons_path = 'format-favicons.txt'

const content_viewport = "width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"
const content_description = "<%= @prop['school_website_description'] %>"
const conten_keywords = "<%= @prop['school_website_keywords'] %>"
const data_title = "<%= @prop['school_website_title'] %>"

const og_title = data_title
const og_type = "website"
const og_description = content_description
const og_url = "<%= @prop['web_url'] %>"
const og_image = `/<%= @prop['web_logo'] %>`
const og_image_alt = data_title
const og_image_type = `image/<%= @prop['web_logo_type'] %>`
const og_image_width = 400
const og_image_height = 400
const og_image_present = true
const theme_color = "<%= @prop['school_website_theme_color'] %>"

fs.readFile(file_path,'utf8',(err,data)=>{
	if(!err){

		data = setTitle(data,data_title)
		data = setMetaTag("name","viewport",content_viewport,data)
		data = setMetaTag("name","description",content_description,data)
		data = setMetaTag("name","keywords",conten_keywords,data)
		data = setMetaTag("name","theme-color",theme_color,data)
		data = setMetaTag("name","apple-mobile-web-app-status-bar-style",theme_color,data)
		data = setMetaTag("name","login_title","<%= @prop['web_login_title'] %>",data)
		data = setMetaTag("name","login_header_title","<%= @prop['web_login_header_title'] %>",data)
		data = setMetaTag("name","login_header_color_title","<%= @prop['web_login_header_color_title'] %>",data)
		data = setMetaTag("name","login_header_background_color_title","<%= @prop['web_login_header_background_color_title'] %>",data)
		data = setMetaTag("property","og:title",og_title,data)
		data = setMetaTag("property","og:type",og_type,data)
		data = setMetaTag("property","og:description",og_description,data)
		data = setMetaTag("property","og:url",og_url,data)
		if(og_image_present){
			data = setMetaTag("property","og:image",og_image,data)
			data = setMetaTag("property","og:image:type",og_image_type,data)
			data = setMetaTag("property","og:image:width",og_image_width,data)
			data = setMetaTag("property","og:image:height",og_image_height,data)
			data = setMetaTag("property","og:image:alt",og_image_alt,data)
		}
		setFavicons(data,(data2)=>{
			data_formatted = formatter.render(data2)
			data_formatted = data_formatted.replace(/%\s+\/\>/g,"%>")
			fs.writeFile(file_path,data_formatted,(err2)=>{
				if(err2){
					throw err2
				}
			})
		})
	}else{
		throw err
	}
})

const setTitle = (data,value)=>{
	const regex = /\<title\>([\w\-]+)\<\/title\>/gi
	const dd = data.replace(regex,`<title>${value}</title>`)
	return dd
}

const setMetaTag = (meta_type,meta_field,meta_value,data)=>{
	const regex_head = /<head>([\s\S]+)<\/head>/
	const regex = new RegExp(`<meta\\s+${meta_type}\\=\\"?${meta_field}\\"?\\s+content\\=\\"([^\\"]+)\\"\\s*\\/>`,'gi')
	const str_replacer = `<meta ${meta_type}=\"${meta_field}\" content="${meta_value}" />`
	
	if(regex.test(data)){
		data = data.replace(regex,str_replacer)
	}else{
		data = data.replace(regex_head,`<head>$1${str_replacer}</head>`)
	}
	return data
}

const setFavicons = (data,callback)=>{

	fs.readFile(file_favicons_path,'utf8',(err2,favicons_txt)=>{
		if(!err2){
			if(!/apple-touch-icon/.test(data)){
				data = data.replace(/<head>([\s\S]+)<\/head>/,`<head>$1${favicons_txt}</head>`)
			}
			callback(data)
		}else{
			throw err2
		}
	})
}