#!/bin/bash
npm run build
node format-dist-index.js
mv dist/index.html dist/index.html.erb

cd ../school-rails

rm public/js/* 
rm public/css/* 
cp -r ../school-vue/dist/* public/ 
cp ../school-vue/config_favicons/* public/
cp public/index.html.erb app/views/intranet/index.html.erb

